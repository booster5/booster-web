# Change Log

## v3.2.0
1. Update to Spring Boot 3.4.2
2. Update to Spring Cloud 2024.0.0
3. Update to Spring Cloud GCP 6.0.0
4. Update to JVM 21
5. Update dependencies

## v3.0.0
1. Change to Spring Boot 3.2.5

## v2.0.0
1. Change base pom and project organization.

## v1.0.0

1. Initial version.
2. Based on Spring Boot 2.7.5.
3. JDK 17.
