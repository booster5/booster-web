# Booster Web

## Purpose

This package provides several easy ways to format a
RESTful service and provide better documentation for 
your RESTful services:

1. Springfox is supported automatically out of box.
2. RESTful endpoints having a ```Mono<Either<Throwable, T>>>``` return types are automatically converted to ```Mono<ResponseEntity<WebResponse<T>>>```.
3. Normal responses and exceptions are handled in a uniform way.

## Usage 

In order to use **booster-web**, one only needs to make sure the RESTful endpoints
return ```Mono<Either<Throwable, T>>``` where T is the actual return value, and any exceptions 
are caught and encapsulated in ```Either```.

The return type is then automatically converted to a ```ResponseEntity<WebResponse<T>>``` object 
and exceptions are used to construct the HTTP status code and detailed error message returned.

A ```WebResponse``` object has the following structure:

```json
{
  "response": {},
  "error": {
    "error_code": "ERROR_CODE_STRING",
    "message": "error message",
    "stack_trace": "stack trace of any exception"
  }
}
```

Either **response** or **error** can be present, but not both present at the same time.

When an ```Either``` has a left value, the exception is extracted, then used to construct 
the error message and HTTP status code returned.

One can choose to wrap a ```Throwable``` in ```WebException```, which allows
one to specify an HTTP status code, which will be used to return to client side,
an error code, and a message for the exception.

If the left value of ```Either``` is not a ```WebException```, HTTP status code 
is always **INTERNAL_SERVER_ERROR**, and so is the **error_code**.

## Getting started

### Project Setup

#### Maven

pom.xml
```xml
<dependency>
  <groupId>io.gitlab.booster</groupId>
  <artifactId>booster-web</artifactId>
  <version>VERSION_TO_USE</version>
</dependency>
```
if you haven't yet set up your repository:
```xml
<repositories>
  <repository>
    <id>gitlab-maven</id>
    <url>https://gitlab.com/api/v4/groups/61046646/-/packages/maven</url>
  </repository>
</repositories>
```

#### Gradle Groovy

```groovy
implementation 'io.gitlab.booster:booster-web:VERSION_TO_USE'
```
setup maven repository:
```groovy
maven {
  url 'https://gitlab.com/api/v4/groups/61046646/-/packages/maven'
}
```

#### Gradle Kotlin

```kotlin
implementation("io.gitlab.booster:booster-web:VERSION_TO_USE")
```
setup maven repository:
```kotlin
maven("https://gitlab.com/api/v4/groups/61046646/-/packages/maven")
```
