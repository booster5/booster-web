package io.gitlab.booster.web.handler.config;

import arrow.core.Either;
import arrow.core.Option;
import com.fasterxml.classmate.TypeResolver;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.type.TypeFactory;
import io.gitlab.booster.web.handler.ExceptionConverter;
import io.gitlab.booster.web.handler.ExceptionHandler;
import io.gitlab.booster.web.handler.ResponseHandler;
import io.gitlab.booster.web.handler.response.WebResponse;
import io.swagger.v3.core.converter.AnnotatedType;
import io.swagger.v3.core.converter.ModelConverter;
import io.swagger.v3.core.converter.ModelConverterContext;
import io.swagger.v3.oas.models.media.Schema;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.http.codec.ServerCodecConfigurer;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.accept.RequestedContentTypeResolver;
import reactor.core.publisher.Mono;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.AlternateTypeRules;
import springfox.documentation.schema.WildcardType;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

import java.util.Iterator;
import java.util.List;

/**
 * Creates beans
 */
@Configuration
public class BoosterWebConfig {

    /**
     * Default constructor
     */
    public BoosterWebConfig() {
    }

    /**
     * Creates {@link ExceptionConverter} to handle exceptions
     * @param handlers {@link List} of {@link ExceptionHandler}s to handle specific {@link Throwable}
     * @return {@link ExceptionConverter} instance
     */
    @Bean
    public ExceptionConverter exceptionConverter(
            @Autowired(required = false)
            List<ExceptionHandler<?>> handlers
    ) {
        return new ExceptionConverter(handlers);
    }

    /**
     * Creates a {@link ResponseHandler} to reformat web endpoint responses
     * @param serverCodecConfigurer {@link ServerCodecConfigurer}
     * @param requestedContentTypeResolver {@link RequestedContentTypeResolver}
     * @param exceptionConverter {@link ExceptionConverter}
     * @return {@link ResponseHandler} instance
     */
    @Bean
    public ResponseHandler responseHandler(
            @Autowired ServerCodecConfigurer serverCodecConfigurer,
            @Autowired RequestedContentTypeResolver requestedContentTypeResolver,
            @Autowired ExceptionConverter exceptionConverter
    ) {
        return new ResponseHandler(
                serverCodecConfigurer.getWriters(),
                requestedContentTypeResolver,
                exceptionConverter
        );
    }

    /**
     * Springfox swagger document generation. Converts
     * Mono&lt;Either&lt;Throwable, Option&lt;T&gt;&gt;&gt;
     * to WebResponse&lt;T&gt; for swagger document.
     * @return {@link Docket}
     */
    @Bean
    @ConditionalOnMissingBean(annotation = EnableSpringDocConverter.class)
    public Docket api() {

        TypeResolver typeResolver = new TypeResolver();

        // annotate only RestControllers, and convert
        // Mono<Either<Throwable, T>> to WebResponse<T>
        return new Docket(DocumentationType.OAS_30)
                .select()
                .apis(RequestHandlerSelectors.withClassAnnotation(RestController.class))
                .paths(PathSelectors.any())
                .build()
                .alternateTypeRules(
                        AlternateTypeRules.newRule(
                                typeResolver.resolve(
                                        Mono.class,
                                        typeResolver.resolve(
                                                Either.class,
                                                Throwable.class,
                                                typeResolver.resolve(
                                                        Option.class,
                                                        WildcardType.class
                                                )
                                        )
                                ),
                                typeResolver.resolve(
                                        WebResponse.class,
                                        WildcardType.class
                                ),
                                Ordered.HIGHEST_PRECEDENCE
                        ),
                        AlternateTypeRules.newRule(
                                typeResolver.resolve(
                                        Either.class,
                                        Throwable.class,
                                        typeResolver.resolve(
                                                Option.class,
                                                WildcardType.class
                                        )
                                ),
                                typeResolver.resolve(
                                        WebResponse.class,
                                        WildcardType.class
                                ),
                                Ordered.HIGHEST_PRECEDENCE
                        )
                );
    }

    /**
     * Springdoc model converter that ignores expands Either&lt;Throwable, Option&lt;T&gt;&gt; to WebResponse&lt;T&gt;
     * @return {@link ModelConverter}
     */
    @Bean
    @ConditionalOnBean(annotation = EnableSpringDocConverter.class)
    public ModelConverter monadModelConverter() {

        return new ModelConverter() {

            @Override
            public Schema resolve(AnnotatedType annotatedType, ModelConverterContext modelConverterContext, Iterator<ModelConverter> iterator) {
                if (isMonoWithEitherOption(annotatedType)) {
                    // Extract the actual type T from Mono<Either<Throwable, Option<T>>>
                    AnnotatedType actualType = extractActualType(annotatedType);

                    // Let the chain handle the resolved type
                    return modelConverterContext.resolve(actualType);
                }

                // Continue with the chain for other types
                return iterator.hasNext() ? iterator.next().resolve(annotatedType, modelConverterContext, iterator) : null;
            }

            private boolean isMonoWithEitherOption(AnnotatedType type) {
                JavaType javaType = (JavaType) type.getType();

                if (javaType.isTypeOrSubTypeOf(Either.class)) {
                    JavaType leftInnerType = javaType.getBindings().getBoundType(0);
                    JavaType rightInnerType = javaType.getBindings().getBoundType(1);

                    // Check if inner type is Either<Throwable, Option<T>>
                    return leftInnerType.isTypeOrSubTypeOf(Throwable.class) &&
                            rightInnerType.isTypeOrSubTypeOf(Option.class);
                }

                return false;
            }

            private AnnotatedType extractActualType(AnnotatedType type) {
                JavaType javaType = (JavaType) type.getType();

                // Navigate through Mono -> Either -> Option -> T
                JavaType optionType = javaType.getBindings().getBoundType(1);
                JavaType actualType = optionType.getBindings().getBoundType(0);

                // Create a JavaType for WebResponse<T>
                TypeFactory typeFactory = TypeFactory.defaultInstance();
                JavaType webResponseType = typeFactory.constructParametricType(WebResponse.class, actualType);

                // Create a new AnnotatedType with the WebResponse<T> type
                return new AnnotatedType()
                        .type(webResponseType)
                        .ctxAnnotations(type.getCtxAnnotations())
                        .parent(type.getParent())
                        .schemaProperty(type.isSchemaProperty())
                        .name(type.getName())
                        .resolveAsRef(type.isResolveAsRef());
            }
        };
    }
}
